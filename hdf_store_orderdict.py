''' A class to store keys in a text file and values in a hdf file.
Assumptions: value.shape is identical for all.
'''
import h5py
import sys
import simplejson
import os
import numpy as np
import pdb
from collections import OrderedDict
from collections import Iterable


FOLDER_DATA = 'data/'
try:
    os.mkdir(FOLDER_DATA)
except OSError:
    pass


def force_list(item):
    # if not isinstance(item, Iterable):
    if type(item) not in [list, tuple]:
        item = [item]
    return item


class HDFStoreOrderdict(OrderedDict):
    def __init__(self, filename, dataset_names, shapes=[]):
        '''
        filename: string, used as a filename.
        dataset_names: list of strings, used when saving into hdf
        shapes : list of tuples
        '''
        if not shapes == []:
            assert len(dataset_names) == len(shapes), \
                ('Length mismatch, %d and %d' % (len(dataset_names), len(shapes)))
        super(HDFStoreOrderdict, self).__init__() 
        self.filename = filename
        self.path_hdf = FOLDER_DATA + self.filename + '.h5'
        self.path_txt = FOLDER_DATA + self.filename + '.txt'
        self.dataset_names = force_list(dataset_names)
        self.shapes = force_list(shapes)


    def __setitem__(self, key, values):
        '''Insert a new value
        values: list of np arrays'''
        def _check_shape(key, value, shape):
            try:
                assert value.shape == shape
                return True
            except:
                print("Unexpected error in insert:", sys.exc_info()[0],
                      ' ..with value to set: ', value, ' key: ', key)
            return False

        # force values to be a list
        values = force_list(values)
        
        for value, shape in zip(values, self.shapes):
            if not _check_shape(key, value, shape):
                raise RuntimeError('Wrong shape')        
        
        super(HDFStoreOrderdict, self).__setitem__(key, values)
        return


    def save(self, overwrite=False):
        '''Store keys and values 
        '''        
        try:
            if overwrite:
                write_flag = 'w'
            else:
                write_flag = 'w-'
            f_hdf = h5py.File(self.path_hdf, write_flag)
        except IOError:
            print('{}already exists'.format(self.path_hdf))
            assert False, 'Saving stopped'
        except:
            print "Unexpected error in saving:", sys.exc_info()[0]
            assert False, 'Saving stopped'

        if not overwrite:
            assert not os.path.exists(self.path_txt), 'txt file exists'

        with open(self.path_txt, 'w') as f_txt:
            simplejson.dump(self.keys(), f_txt)

        for dataset_idx, (shape, dataset_name) in enumerate(zip(self.shapes, 
                                                       self.dataset_names)):
            d_shape = (self.__len__(), ) + shape
            f_hdf.create_dataset(dataset_name, d_shape, dtype='f')
            for idx, values in enumerate(self.itervalues()):
                f_hdf[dataset_name][idx] = values[dataset_idx]
        f_hdf.close()
        return


    def loadable(self):
        cond1 = os.path.exists(self.path_txt)
        cond2 = os.path.exists(self.path_hdf)
        return cond1 and cond2


    def load(self):
        if not self.loadable():
            raise RuntimeError('No file to load from!')

        keys = simplejson.load(open(self.path_txt, 'r'))
        with h5py.File(self.path_hdf, 'r') as f_hdf:
            f_dsets = [f_hdf[ds_name] for ds_name in self.dataset_names]
            for key_idx, key in enumerate(keys):
                self.__setitem__(key, [f_d[key_idx] for f_d in f_dsets])
        # set self.shapes
        if self.shapes is []:
            self.shapes = [f_d[0].shape for f_d in f_dsets]
        return


def test():
    hsd = HDFStoreOrderdict('test_filename', ['val1'], [(10, )])
    hsd['a'] = [np.zeros((10, ))]
    hsd['b'] = [np.zeros((10, ))]
    hsd['c'] = [np.zeros((10, ))]
    hsd['d'] = np.zeros((10, ))
    hsd['e'] = np.zeros((10, ))
    try:
        hsd['f'] = np.zeros((11, ))
    except:
        'Wrong input shape!'

    hsd.save()

    hsd2 = HDFStoreOrderdict('test_filename', ['val1'])
    hsd2.load()
    print(hsd2)
    pdb.set_trace()


if __name__ == '__main__':
    test()

