''' Find nearest neighbors based on audio features.
It uses pyflann package.
'''
import numpy as np
import pyflann
import cPickle as cP
from collections import Iterable
import pdb

PATH_DATA = 'data/' # path that has audio feature file
FILE_FEAT = 'original_feat_20161001.cP' # audio feature file name

class Audio_NN():
    def __init__(self):
        self.ids, self.orig_feats = self._load_feats() # song ids, features
        self.num_songs = len(self.orig_feats) # number of songs
        self.feat_dim = len(self.orig_feats[0]) # feature dimension

    def idx_to_id(self, idx): # song index to song id
        try:
            return self.ids[idx] 
        except:
            return None

    def id_to_idx(self, song_id): # song id to song index
        try:
            return self.ids.index(song_id)
        except:
            return None
    
    def _load_feats(self):
        '''Load feature dictionary, make it into list, return it with id.'''
        orig_feats_dict = cP.load(open(PATH_DATA + FILE_FEAT, 'r'))
        file_ids = []
        orig_feats = []
        for idx_file, (file_id, feat) in enumerate(orig_feats_dict.iteritems()):
            file_ids.append(file_id)
            orig_feats.append(feat)
        orig_feats = np.array(orig_feats)
        return file_ids, orig_feats

    def nn_by_feats(self, feats, n_neighbors=5):
        '''find NN by feature
        Arguments
        ---------
        feats: numpy 2d array size of (n_query_songs, self.feat_dim)

        Return
        ------
        (indices of nn, corresponding distance), 
        np.ndarray, shape: (len(feats), n_neighbors)
        '''
        kwargs={'algorithm':'kmeans',
            'branching':32,
            'iteration':7,
            'checks':16}
        flann = pyflann.FLANN()
        idx_results, dists = flann.nn(
            self.orig_feats, feats, n_neighbors+1, **kwargs)
        idx_results = idx_results[:, 1:] # remove myself
        dists = dists[:, 1:]

        id_results = []
        for idx_result in idx_results:
            id_results.append([self.ids[idx] for idx in idx_result])

        return id_results, idx_results, dists

    def nn_by_idxs(self, idxs, n_neighbors=5):
        '''find NN by idxs in [0, self.num_songs].
        Arguments
        ---------
        idxs : integer or array of integers
            indices of query songs
            e.g. 1, 54354, [1, 324, 234]

        n_neighbors : integer, number of neighbors to get

        Return
        ------ 
        see return value of self.nn_by_feats()'''
        if not isinstance(idxs, Iterable): # force to be a list
            idxs = [idxs]

        for idx in idxs:
            assert idx >= 0 and idx < self.num_songs, \
                ('wrong song idx, %d' % idx)

        feats = np.array([self.orig_feats[i] for i in idxs])
        return self.nn_by_feats(feats, n_neighbors)

    def nn_by_ids(self, ids, n_neighbors=5):
        '''find NN by id.

        Arguments
        ---------
        ids : song id or array of song ids
            e.g. '7yeEIxH5WYPjqhW2xHMbpL', ['7yeEIxH5WYPjqhW2xHMbpL']

        n_neighbors : integer, number of neighbors to get

        Return
        ------
        see return value of self.nn_by_feats()
        '''
        if not isinstance(ids, Iterable):
            ids = [ids]
        idxs = [self.id_to_idx(song_id) for song_id in ids]

        return self.nn_by_idxs(idxs, n_neighbors=n_neighbors)


def test(test_size=5):
    print('initializing audio nn...')
    np.random.seed(1209)
    audio_nn = Audio_NN()
    print('testing...')
    test_idxs = np.random.randint(low=0, 
                                 high=audio_nn.num_songs, 
                                 size=test_size)
    print('nn by idx')
    ids, idxs, dists = audio_nn.nn_by_idxs(test_idxs)
    for song_id, idx, test_idx in zip(ids, idxs, test_idxs):
        print('seed index:{}, results: {}\n{}'.format(str(test_idx), idx, song_id))
    print('nn by id')
    test_ids = [audio_nn.idx_to_id(idx) for idx in test_idxs]
    ids, idxs, dists = audio_nn.nn_by_ids(test_ids)
    for song_id, idx, test_idx in zip(ids, idxs, test_idxs):
        print('seed index:{}, results: {}\n{}'.format(str(test_idx), idx, song_id))

    return


if __name__ == '__main__':
    test()
    '''
$ python main_audio_feat_nn.py
initializing audio nn...
testing...
seed index:10962, results: [  1159  27654 159886 150476 121840]
['7yeEIxH5WYPjqhW2xHMbpL', '0CYoCmKmQleDxfv0afAivT', '6dkOQdVmO2nCWotgXbGEHg', '2RR4BNlpqmj16FHANb7IqV', '0elXIl26ZrovBULLTrEGK6']
seed index:40579, results: [ 16417 127033 129185 137619 100628]
['6ele0FaYh00ngGE56lS2qf', '3J3pbwzKzBpvEOykciz9do', '4QOD0P7ZXSCwdmjGGIiO45', '5slL1CvkcNbVoEhBfvGn3c', '3mz0hgzGaDXxiLWzcDGfa8']
seed index:47231, results: [ 27813  74926 170928 106128 158985]
['58XBd02hOUfwVE69Pw6y7m', '2xtzIMCM6Jf2QlfQfm31He', '3vhxkxBattMsLXgmTuCwlZ', '0EdfeUG4DyJcNs8MN5mEeS', '4c0ecVwn17owdwpIBL07wC']
seed index:136186, results: [ 32260  64558  91465 127091 136186]
['1Cv2OxsAoAZqXzM673mWAG', '7sUKLjQlZy7MbIl4CJz4Eu', '6PjnzPDZXn3MJvcbMiyYbu', '7cDRp1z66mthBlsDVhWNJR', '6AqxYCmYLvFZdIdUL6igOY']
seed index:65206, results: [  3678 127771 116561  36873  74653]
['0xtkk7hlDJbAzE7S5G3YK6', '00liNdPiIHYmt3IgVwm647', '299SmczRr5gLmKGF4BytyD', '57Y3UccJEJqT8w8RWkUAz0', '7aoHi8U1sFJIWUReY8vphS']
seed index:93471, results: [101807  86460 119978 116850  10950]
['2QZ6ahaUGPJFLvCzEUxS4a', '0bXyVZ2wH8sYzsbHAOnaGK', '6Hm0mU868o8cPVkQ7tOmFa', '4mwvz3MqMTNLMxuIHeJbGd', '2I75GCnEOzCvNpxCR3XYZH']
seed index:118619, results: [ 82512 147757   2368 119555 134639]
['5pyB5WEAEwqwl18UaBLeBl', '05VJNnuJwCXlxLlYXvDE1x', '6oEVChvTrCiFngNmFzCsTL', '6x5BI1UbcFoxdnl8XlmzvF', '1osIRm1CFQVJHsBiwtvZTF']
    '''
