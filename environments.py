'''global variables for path and etc. '''

PATH_BUZZ = '/misc/kcgscratch1/ChoGroup/keunwoo/Gnubox/buzzmusiq/'
PATH_MP3 = PATH_BUZZ + 'download_audio_preview/mp3-preview/'
PATH_NPY = PATH_BUZZ + 'download_audio_preview/npy-preview/'

PATH_DATA = 'data/'

SR = 12000

try:
    os.mkdir(PATH_DATA)
except OSError:
    pass
