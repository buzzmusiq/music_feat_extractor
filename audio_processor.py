import librosa
import numpy as np
from environments import *


N_FFT = 512
N_MELS = 96
HOP_LEN = 256
DURA = 29.12  # to make it 1366 frame..

MIN_TEMPO = 60 
MAX_TEMPO = 140 # mid-tempo == 100

def compute_melgram_tempo(npy_path):
    try:
        src = np.load(npy_path)
        try:
            melgram = get_melgram(src)
        except:
            melgram = np.zeros((1, 96, 1366))
        
        try:
            tempo = get_tempo(src)
        except:
            tempo = np.min((100, 100))

    except:
        melgram = np.zeros((1, 96, 1366))
        tempo = np.min((100, 100))
    
    return melgram, tempo


def load_audio(audio_path):
    src, sr = librosa.load(audio_path, sr=SR)  # whole signal
    return src


def get_melgram(src):
    ''' Compute a mel-spectrogram and returns it in a shape of (1, 96, 1366)'''
    try:
        n_sample = src.shape[0]
        n_sample_fit = int(DURA*SR)

        if n_sample < n_sample_fit:  # if too short
            src = np.hstack((src, np.zeros((int(DURA*SR) - n_sample,))))
        elif n_sample > n_sample_fit:  # if too long
            src = src[(n_sample-n_sample_fit)/2:(n_sample+n_sample_fit)/2]
        logam = librosa.logamplitude
        melgram = librosa.feature.melspectrogram
        ret = logam(melgram(y=src, sr=SR, hop_length=HOP_LEN,
                            n_fft=N_FFT, n_mels=N_MELS)**2,
                    ref_power=1.0)
        ret = ret[np.newaxis, :]

        return ret
    except:
        return np.zeros((1, 96, 1366))


def compute_melgram(audio_path):
    ''' Compute a mel-spectrogram and returns it in a shape of (1, 96, 1366), where
    96 == #mel-bins and 1366 == #time frame

    parameters
    ----------
    audio_path: path for the audio file.
                Any format supported by audioread will work.
    More info: http://librosa.github.io/librosa/generated/librosa.core.load.html#librosa.core.load

    '''

    # mel-spectrogram parameters
    
    src = load_audio(audio_path)
    ret = get_melgram(src)
    return ret


def get_tempo(src):
    ''' use librosa tempo estimator. assumes sr=SR '''
    onset_env = librosa.onset.onset_strength(src, sr=SR)
    try:
        tempo = librosa.beat.estimate_tempo(onset_env, sr=SR)
    except IndexError: # it occurs when len(src) is too short.
        tempo = 100
    except:
        print('Weird on tempo estimation!')
        pdb.set_trace()
    return np.min((np.max((tempo, MIN_TEMPO)), MAX_TEMPO))

