import sys
import os
import h5py
import time
import logging
import pdb

import numpy as np
import cPickle as cP
import audio_processor as ap
import cnn_model

from joblib import Parallel, delayed
from multiprocessing import Pool
from sklearn.decomposition import PCA
from environments import *
from hdf_store_orderdict import HDFStoreOrderdict


N_JOB = 8 # Number of jobs for multi-processing feature extraction.
N_TOTAL_TRACKS = 3838313 # number of tracks to process 
# Set up a logger
logging.basicConfig(filename='log_main_feat_extract.log', level=logging.DEBUG,
	format='%(asctime)s %(levelname)s : %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
logging.getLogger().addHandler(logging.StreamHandler())

# Track id list
FILES_LIST = PATH_DATA + 'preview_ids_20161018.txt' # all id (id's of spotify preview files + apple prefiew files)
		  #  'target_tracks_20160922.txt': 221981 songs
# FILE_AUDIO_FEAT = 'audio_feature' # NOT USED
# FILE_AUDIO_TEMPO = 'audio_temp' # NOT USED
# FILE_FEAT_PCA = 'PCAed_feats.cP' # NOT USED
# FILE_FAIL_TXT = 'original_failures.txt' # NOT USED

def npy_exist(file_id):
	'''check if a npy file exists. 
	Each numpy file contains a numpy array of a decoded audio track
		of `fild_id`. '''
    return os.path.exists(PATH_NPY + file_id + '.npy') and os.path.getsize(PATH_NPY + file_id + '.npy') > 0


def file_id_generator(data_dict):
	'''generate file id that is undone yet according to data_dict '''
	with open(FILES_LIST) as f_list:
		for line in f_list:
			file_id = line.rstrip('\n')
			if file_id not in data_dict:
				yield file_id # yield only undone
	return


def path_batch_generator(data_dict, batch_size):
	'''generate ids and paths for each batch. '''
	id_gen = file_id_generator(data_dict)
	cond_while = True
	while cond_while:
		ids = []
		paths = []
		for _ in xrange(batch_size):
			try:
				file_id = id_gen.next()
				ids.append(file_id)
				paths.append(PATH_NPY + file_id + '.npy')
			except StopIteration:
				cond_while = False
				break
		yield ids, paths
	return


def init_data_dict(filename):
	'''Inits a HDFStoreOrderdict instance with datasets of
		`'audio_feature'` and `'tempo'`, each size is
		`(32, )` and `()`. 
	'''
    hsd = HDFStoreOrderdict(filename, ['audio_feature', 'tempo'], [(32,), ()])
    if hsd.loadable():
    	hsd.load()
    return hsd


def main_ext_feature(batch_size, dict_filename):
	def _put_into_dict(data_dict, file_id_queue, features, tempos):
		for file_id, feature, tempo in zip(file_id_queue, features, tempos):
			data_dict[file_id] = [feature, tempo]
		return

	'''Main function to extract music features

	# Arguments
		batch_size: integer, size of each batch.

	# dict_filename: the filename of `HDFStoreOrderdict` to store the
		extracted audio features.
	'''
	logging.info('='*40)
	logging.info('Starting new log for audio feature and tempo extraction.')
	logging.info('feature extraction with batch_size=%d' % batch_size)

	model = cnn_model.model() # Create CNN model
	model.load_weights('cnn_model/weights_best.hdf5', by_name=True) # Load weights
	logging.info('CNN model is loaded')

	data_dict = init_data_dict(dict_filename) # Initiate a HDFStoreOrderdict
	gen_path = path_batch_generator(data_dict, batch_size) # Ready to go.

	pool = Pool(N_JOB)
	# run!
	start_time = time.time()
	tempos = np.zeros((batch_size, ))
	logging.info('Melgram and tempo estimation!')	
	total_batch_est = (N_TOTAL_TRACKS - len(data_dict)) / batch_size
	for batch_idx, (ids, paths) in enumerate(gen_path): # For each batch,
		loop_start_time = time.time()		
		try:
			# First, get melgrams and tempos.
			melgrams_tempo_tuples = pool.map(ap.compute_melgram_tempo, paths)
		except: # There could be some invalid audio files..
			data_dict.save(overwrite=True)
			for path_subidx, path in enumerate(paths):
				try:
					melgram, tempo = ap.compute_melgram_tempo(path)
				except:
					print('Ha, loading and computing melgram failed.')
					print('The problem is at %d' % path_subidx)
					print('%s' % path)
					pdb.set_trace()
			print('loop for debug is done.')
			pdb.set_trace()
		
		melgrams, tempos = zip(*melgrams_tempo_tuples)		
		melgrams = np.array(melgrams)
		# Second, extract audio features using melgrams.
		features = model.predict(melgrams) # shape: batch_size x 32
		# Store the features and tempo into HDFStoreOrderdict
		_put_into_dict(data_dict, ids, features, tempos)
		
		time_loop = time.time() - loop_start_time
		print('%d/%d-th batch : done, took %d seconds, will take about %4.2f hrs.' % \
			  (batch_idx, total_batch_est, time_loop,
			  	time_loop * (total_batch_est - batch_idx) / 3600.0 ))

		# UNCOMMENT THIS IF YOU WANNA SAVE THE INTERMEDIATE RESULTS
		# save_start_time = time.time()
		# if batch_idx % (total_batch_est/2) == 0: # save it just in case...
		# 	data_dict.save(overwrite=True)
		# 	print('...was busy on saving for %d s.' % (time.time() - save_start_time))
				
	# logging
	logging.info('feature extraction is done for %5.3f seconds' % (time.time() - start_time))
	logging.info('in total, %d song features were extracted' % len(data_dict))

	sys.stdout.write('\rFinal saving...')
	# Finally, store the dictionary.
	data_dict.save(overwrite=True)

	return data_dict


def post_process(hdf_path_read, hdf_path_new):
	'''Load a hdf files, whiten the data, then store it into a new hdf file.'''
	logging.info('Start post-processing')
	with h5py.File(hdf_path_read, 'r') as f_hdf:
		with h5py.File(hdf_path_new, 'w') as f_hdf_w:
			for dset_name in f_hdf:
				f_hdf_w.create_dataset(dset_name, 
									   shape=f_hdf[dset_name].shape, 
									   dtype='f')
				_whiten(f_hdf[dset_name], f_hdf_w[dset_name])
				logging.info('..._whiten()')
	logging.info('Post-processing finished')
	return


def _whiten(f_read, f_write):
	'''f_dataset should be appendable open with 'a' '''
	mean = np.mean(f_read, axis=0)
	std = np.std(f_read, axis=0)
	if not np.isscalar(mean):
		mean = mean[np.newaxis, :]
		std = std[np.newaxis, :]

	f_write = (f_read - mean) / std
	return


def _pca(f_read, f_write, dim_PCA):
	'''PCA and fill zeros for the others
	input: hdf file pointer to the dataset
	'''
	num_orig_files, dim_orig = f_read.shape
	# dim_PCA = 32 # 32 to 16

	logging.info('PCA starts for dim %d to %d with whitening' % (dim_orig, dim_PCA))

	# do PCA
	pca = PCA(n_components=dim_PCA, whiten=True)
	PCAed_features = pca.fit_transform(f_read)
	# write new one - pca features
	f_write = PCAed_features
	logging.info('PCA done!')
	return


def test_path_gen(dict_filename):
	data_dict = init_data_dict(dict_filename)
	gen_path = path_batch_generator(data_dict, batch_size)
	for batch_idx, (ids, paths) in enumerate(gen_path):
		print('batch_idx : %d, %d ids, %d paths' % (batch_idx, len(ids), len(paths)))
	print('Test finished.')
	pdb.set_trace()
	return

if __name__ == '__main__':

	batch_size = 2**12 # 6:108, 8:20
	machine_idx, num_machine = 0, 1
	dict_filename = 'audio_feature_4M_%dof%d' % (machine_idx, num_machine)
	data_dict = main_ext_feature(batch_size, dict_filename)
	post_process(data_dict.path_hdf, PATH_DATA + dict_filename + '_postprocessed.h5')

