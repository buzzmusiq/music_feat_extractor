'''keras 1.0.8 compatible'''
import sys
import numpy as np
import keras
from keras.models import Sequential
from keras.layers import Layer, Dense, Dropout, Activation, Flatten
from keras.models import Model
from keras.layers import merge, Input
from keras.layers.convolutional import Convolution2D, MaxPooling2D, AveragePooling2D
from keras.optimizers import RMSprop, SGD
from keras.layers.normalization import BatchNormalization
# for graph
from keras.models import Model
from keras.layers.convolutional import ZeroPadding2D
from keras.layers import Reshape, Permute


conv2d_buzz = {'conv_sizes' : [(3,3), (3,3), (3,3), (3,3), (3,3)],
            'pool_sizes' : [(2,4), (2,4), (2,4), (3,5), (4,4)],
            'nums_feat_maps' : [32, 40, 48, 40, 32],
            'num_conv_layers' : 5,
            'num_fc_layers' : 0,
            'num_units_fc' : 0,
            'dropout_conv' : 0.0,
            'dropout_fc' : 0.0,
            'model_type' : 'vgg_simple',
            'feat_scale_factor' : 1.0,
            'memo' : '___buzzmusiq_fast_conv2d_last_32___',
            'dropout_rnn' : None,
            'dropout_rnn_output' : None,
            'num_rnn_layers' : 0,
            'num_hidden_rnn' : None,
            'nb_epoch' : 30,
            'early_stop' : True,
            'early_stop_threshold' : 0.0,
            'early_stop_patience' : 80,
            'batch_size' : 32}


def model():
    num_channels = 1
    height_input = 96
    width_input = 1366
    num_conv_layers = 5
    nums_feat_maps = [32, 40, 48, 40, 32]
    feat_scale_factor = 1.0
    conv_sizes = [(3,3), (3,3), (3,3), (3,3), (3,3)]
    pool_sizes = [(2,4), (2,4), (2,4), (3,5), (4,4)]
    dropout_conv = 0.0
    
    # prepre modules
    model = Sequential()
    model.add(BatchNormalization(axis=2, mode=2, 
                                 input_shape=(num_channels, height_input, width_input))) # per each frequency.
    args = [num_conv_layers, nums_feat_maps, feat_scale_factor, conv_sizes, \
            pool_sizes, dropout_conv, (num_channels, height_input, width_input)]
    model.add(get_convBNeluMPdrop(*args, num_nin_layers=1))
    
    model.add(Flatten()) # use model.output_shape (which is e.g. (None, 32)) for the following layer
    return model    


def get_convBNeluMPdrop(num_conv_layers, nums_feat_maps, feat_scale_factor, 
        conv_sizes, pool_sizes, dropout_conv, input_shape, num_nin_layers=1,
        batch_normalization_conv=True, name='ConvBNEluDr', bn_mode=2, **kwargs):

    #[Convolutional Layers]
    model = Sequential(name=name)
    input_shape_specified = False
    for conv_idx in xrange(num_conv_layers):
        # add conv layer
        n_feat_here = int(nums_feat_maps[conv_idx]*feat_scale_factor)
        for _ in xrange(num_nin_layers):
            if not input_shape_specified:
                model.add(Convolution2D(n_feat_here, conv_sizes[conv_idx][0], conv_sizes[conv_idx][1], 
                                        input_shape=input_shape,
                                        border_mode='same',  
                                        init='he_normal'))
                input_shape_specified = True
            else:
                model.add(Convolution2D(n_feat_here, conv_sizes[conv_idx][0], conv_sizes[conv_idx][1], 
                                        border_mode='same',
                                        init='he_normal'))
            # add BN, Activation, pooling, and dropout
            if batch_normalization_conv:
                model.add(BatchNormalization(axis=1, mode=bn_mode))
            model.add(keras.layers.advanced_activations.ELU(alpha=1.0)) # TODO: select activation
        
        model.add(MaxPooling2D(pool_size=pool_sizes[conv_idx]))
        if not dropout_conv == 0.0:
            model.add(Dropout(dropout_conv))
    return model

